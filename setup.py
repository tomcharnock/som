import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="SOM",
    version="0.1",
    author="Tom Charnock",
    author_email="tom@charnock.fr",
    description="Self organising map using JAX",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/tomcharnock/SOM.git",
    packages=["SOM"],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3",
    install_requires=[
        "jax",
        "tqdm"]
)
