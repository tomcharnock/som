# Self organising map using jax

An ongoing implementation of a self organising map with a Gaussian neighbourhood function, Gaussian map initialisation and several metrics.

Works on any shaped data with any shaped map.

```python
som = SOM(rng, data_shape, map_shape=(50, 50), σ0=25., α0=5., λ=100., at_once=1, loc=0., scale=1., metric="euclidean")
```
- `rng` - JAX random number generator
- `data_shape` - tuple: shape of input data
- `map_shape` - tuple: shape of output map
- `σ0` - float: initial width of the neighbourhood function
- `α0` - float: initial amplitude of the neighbourhood function
- `λ` - float: learning rate
- `at_once` - int: number of data to process at once (averaged over)
- `loc` - float: mean value of the weight map
- `scale` - float: mean standard deviation of the weight map
- `metric` - str: metric to measure difference between weight map and data vectors, either `euclidean`, `manhattan`, `chebyshev` or `cosine`

```python
som.fit(data, key=None, max_epochs=None)
```
- `data` - float array: input data with shape (n_data,) + data_shape
- `key` - JAX random number generator: needed if the data is selected randomly rather than run through sequentially
- `max_epochs` - int: needed if several passes through the entire data are required. Note the neighbourhood function expands again on each iteration

```python
import matplotlib.pyplot as plt
plt.imshow(som.u_matrix)
```
![](figures/SOM.png)


# To do
- Write comments
- Implement more metrics
- Write README