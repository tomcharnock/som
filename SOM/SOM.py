__author__="Tom Charnock"
__version__="0.1"

import jax
import jax.numpy as np
import tqdm.notebook as tqdm

class SOM:
    def __init__(self, rng, data_shape, map_shape=(50, 50), σ0=25., α0=5., 
                 λ=100., at_once=1, loc=0., scale=1., metric="euclidean"):
        self.data_shape = data_shape
        self.data_len = len(data_shape)
        self.map_shape = map_shape
        self.map_len = len(map_shape)
        self.σ0 = σ0
        self.α0 = α0
        self.λ = λ
        self.at_once = at_once
        self.W = self.initialise_weight_matrix(rng, loc=loc, scale=scale)
        self.neighbourhood_matrix = self.get_neighbourhood_matrix()
        if metric == "euclidean":
            self.metric = self.euclidean
        elif metric == "manhattan":
            self.metric = self.manhattan
        elif metric == "chebyshev":
            self.metric = self.chebyshev
        elif metric == "cosine":
            self.metric = self.cosine
        else:
            print("Only euclidean, manhattan, chebyshev and cosine metrics are defined. " +
                  "Defaulting to euclidean.")
        
    def initialise_weight_matrix(self, key, loc=0., scale=1.):
        return loc + scale * jax.random.normal(
            key, 
            shape=self.map_shape + self.data_shape)
        
    def get_neighbourhood_matrix(self):
        neighbourhood_matrix = np.expand_dims(
            np.arange(
                -self.map_shape[0], 
                self.map_shape[0],
                dtype=float), 
            tuple(np.arange(self.map_len)[np.arange(self.map_len) != 0]))**2.
        for i, size in enumerate(self.map_shape[1:]):
            neighbourhood_matrix += np.expand_dims(
                np.arange(-size, size, dtype=float), 
                tuple(np.arange(self.map_len)[
                    np.arange(self.map_len) != i+1]))**2.
        return np.sqrt(neighbourhood_matrix)
    
    def difference(self, x, y):
        return (np.expand_dims(x, tuple(np.arange(self.map_len))) 
                - np.expand_dims(y, self.map_len))
    
    def euclidean(self, x, y):
        diff = self.difference(x, y)
        return (
            np.sqrt(np.square(diff).sum(
                tuple(np.arange(
                    self.map_len+1, 
                    self.map_len+self.data_len+1)))), 
            diff)
    
    def manhattan(self, x, y):
        diff = self.difference(x, y)
        return (
            np.absolute(diff).sum(
                tuple(np.arange(
                    self.map_len+1, 
                    self.map_len+self.data_len+1))),
            diff)
    
    def chebyshev(self, x, y):
        diff = self.difference(x, y)
        return (
            np.absolute(diff).max(
                tuple(np.arange(
                    self.map_len+1, 
                    self.map_len+self.data_len+1))),
            diff)
    
    def cosine(self, x, y):
        A = np.expand_dims(
                np.sqrt(np.square(x).sum(
                    tuple(np.arange(1, self.data_len+1)))), 
            tuple(np.arange(self.map_len)))
        B = np.expand_dims(
                np.sqrt(np.square(y).sum(
                    tuple(np.arange(
                        self.map_len, 
                        self.map_len+self.data_len)))), 
            self.map_len)
        AB = (np.expand_dims(x, tuple(np.arange(self.map_len))) * 
              np.expand_dims(y, self.map_len)).sum(
            tuple(np.arange(
                self.map_len+1, 
                self.map_len+self.data_len+1)))
        return 1 - np.arccos(AB / (A * B)) / np.pi, self.difference(x, y)

    def get_width(self, i):
        return 2. * (self.σ0 * np.exp(- i / self.λ))**2.
    
    def get_rate(self, i):
        return self.α0 * np.exp(- i / self.λ)
        
    def get_neighbourhood_function(self, dists, i):
        σ = self.get_width(i)
        return np.exp(- dists / σ)
    
    def get_min(self, d):
        return np.array(np.unravel_index(d.argmin(), d.shape))
        
    def get_best_matching_unit(self, data, W):
        d, diff = self.metric(data, W)
        min_ind = jax.vmap(self.get_min, in_axes=-1)(d)
        dists = np.concatenate([
             jax.lax.dynamic_slice(
                 self.neighbourhood_matrix,
                 np.array(self.map_shape) - min_ind[j],
                 self.map_shape)[..., np.newaxis]
             for j in range(min_ind.shape[0])],
            axis=-1)
        return dists, diff
      
    @jax.partial(jax.jit, static_argnums=0)
    def update_weight_map(self, i, data, W):
        dists, diff = self.get_best_matching_unit(data, W)
        β = self.get_neighbourhood_function(dists, i)
        α = self.get_rate(i)
        return (np.expand_dims(W, self.map_len) + 
                np.expand_dims(
                    α * β, 
                    tuple(
                        np.arange(
                            self.map_len+1, 
                            self.map_len+self.data_len+1))) * diff).mean(
            self.map_len)
    
    def check_shape(self, n_data):
        if (n_data / self.at_once) == float(n_data // self.at_once):
            return n_data
        else:
            valid_data = n_data // self.at_once * self.at_once
            string = ("{} inputs were passed, but this will not ".format(n_data) +
                " broadcast with the {} batches which are processed ".format(self.at_once) +
                " at once.. Dropping last {} input".format(n_data - valid_data))
            if n_data - valid_data == 1:
                string += "."
            else:
                string += "s."
            print(string)
            return valid_data
        
    def get_inds(self, n_data, key=None):
        if key is None:
            rng = None
        else:
            rng, key = jax.random.split(key)
        inds = np.arange(n_data)
        if key is not None:
            inds = jax.random.permutation(key, inds)
        return inds.reshape((-1, self.at_once)), rng
    
    def fit(self, data, key=None, max_epochs=None):
        n_data = self.check_shape(data.shape[0])
        if max_epochs is None:
            max_epochs = 1
        bar = tqdm.trange(max_epochs, desc="Epochs")
        for epoch in bar:
            inds, key = self.get_inds(n_data, key=key)
            bar = tqdm.tqdm(enumerate(inds), desc="Iterations")
            for i, ind in bar:
                W = self.update_weight_map(i, data[ind], self.W)
                if np.any(np.isinf(W)):
                    print("inf detected in weight map.")
                    break
                elif np.any(np.isnan(W)):
                    print("NaN detected in weight map.")
                    break
                else:
                    self.W = W
        self.get_u_matrix()
                
    def get_u_matrix(self):
        self.u_matrix = ((self.W[tuple(slice(1, size+1, 1) for size in self.map_shape)] - 
                          self.W[tuple(slice(0, size-1, 1) for size in self.map_shape)])**2.).sum(
            tuple(np.arange(self.map_len, self.map_len+self.data_len)))